import {Injectable} from "@angular/core";
import {RestDataSource} from "../model/rest.dataSource";
import {Observable} from "rxjs";
import {NgForm} from "@angular/forms";
import {Router} from "@angular/router";

@Injectable()
export class AuthService {
  constructor(private datasource: RestDataSource,
              private router: Router) {
  }

  authenticate(form: NgForm): Observable<boolean> {
    return this.datasource.authenticate(form);
  }

  get authenticated(): boolean {
    return this.datasource.auth_token !== null;
  }

  clear(): void {
    this.datasource.auth_token = null;
    this.router.navigateByUrl("/store",{skipLocationChange: true}).then( () => {}, () => {});
  }

}

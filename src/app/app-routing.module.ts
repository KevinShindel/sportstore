import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {StoreComponent} from "./components/store/store.component";
import {CartComponent} from "./components/cart/cart.component";
import {CheckoutComponent} from "./components/checkout/checkout.component";
import {StoreFirstGuard} from "./guards/store.guard";

const routes: Routes = [
  {path: "store", component: StoreComponent, canActivate: [StoreFirstGuard]},
  {path: "cart", component: CartComponent, canActivate: [StoreFirstGuard]},
  {path: "checkout", component: CheckoutComponent, canActivate: [StoreFirstGuard]},
  {path: "admin", // Lazy Module Loading
    loadChildren: () => import("./components/admin/admin.module").then(module => module.AdminModule),
    canActivate: [StoreFirstGuard]}, // Tell to router, after user go to admin page, load router from AdminModule
  {path: "**", redirectTo: '/store'},
];

@NgModule({
  providers: [StoreFirstGuard],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

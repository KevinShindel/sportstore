import { Injectable } from "@angular/core";
import { Product } from "../model/product.model";
import {StaticDataSource} from "../model/static.dataSource.service";

@Injectable()
export class ProductRepository {
  private products: Product[] = [];
  private categories: any[] = [];

  constructor(private dataSource: StaticDataSource) {
    dataSource.getProducts().subscribe(data => {
      this.products = data;
      this.categories = data.map(p => p.category)
        .filter((c, index, array) => array.indexOf(c) == index).sort();
    });
  }

  getProducts(category: any): Product[] {
    return this.products
      .filter(p => category == null || category == p.category);
  }

  // getProduct(id: number): Product {
  //   return this.products.find(p => p.id == id);
  // }

  getCategories(): any[] {
    return this.categories;
  }
}

module.exports = function () {
  return {
    products: [
      {id: 1, name: "Kayak", category: "Watersports", description: "", price: 275},
      {id: 2, name: "Lifejacket", category: "Watersports", description: "", price: 48.95},
      {id: 3, name: "Soccer Ball", category: "Soccer", description: "", price: 18.5},
      {id: 4, name: "Corner flags", category: "Soccer", description: "", price: 34.95},
      {id: 5, name: "Stadium", category: "Soccer", description: "", price: 79500},
      {id: 6, name: "Thinking Cap", category: "Chess", description: "", price: 16},
      {id: 7, name: "Unsteady Chair", category: "Chess", description: "", price: 29.95},
      {id: 8, name: "Human Chess Board", category: "Chess", description: "", price: 75},
      {id: 9, name: "Bling Bling King", category: "Chess", description: "", price: 1200},
      ],
    orders: []
  }
}

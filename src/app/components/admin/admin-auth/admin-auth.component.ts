import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {NgForm} from "@angular/forms";
import {AuthService} from "../../../services/auth.services";

@Component({
  selector: 'app-admin-auth',
  templateUrl: './admin-auth.component.html',
  styleUrls: ['./admin-auth.component.css']
})
export class AdminAuthComponent implements OnInit{
  public username: string = '';
  public password: string = '';
  public errorMessage: string = '';

  constructor(private router: Router, private auth: AuthService) { }

  authenticate(form: NgForm): void {
    if (form.valid) {
      this.auth.authenticate(form).subscribe(response => {
        if (response) {
          this.router.navigateByUrl("/admin/main");
        } else {
          this.errorMessage = 'Authentication Failed!';
        }
      })

    } else {
      this.errorMessage = 'Form Data Invalid';
    }
  }

  ngOnInit(): void {
    if (this.auth.authenticated) this.router.navigateByUrl("/admin/main", {}).then();
  }

}

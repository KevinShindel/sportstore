import {Component, ViewEncapsulation} from "@angular/core";
import {ProductRepository} from "../../repository/product.repository";
import {Product} from "../../model/product.model";
import {Cart} from "../../model/cart.model";
import {Router} from "@angular/router";

@Component(
  {
    selector: "app-store",
    templateUrl: "store.component.html",
    styleUrls: ["store.component.css"],
    encapsulation: ViewEncapsulation.None
  })

export class StoreComponent {
  public selectedCategory = undefined;
  public itemsPerPage = 4;
  public selectedPage= 1;

  constructor(private repository: ProductRepository, private cart: Cart, private router: Router) {}

  get products(): Product[] {
    const pageIdx = (this.selectedPage - 1) * this.itemsPerPage;
    return this.repository.getProducts(this.selectedCategory).slice(pageIdx, pageIdx + this.itemsPerPage);
  }

  get categories() {
    return this.repository.getCategories();
  }

  changeCategory(newCategory = undefined) {
    this.selectedPage = 1;
    this.selectedCategory = newCategory;
  }

  changePage(newPage: number): void {
    this.selectedPage = newPage;
  }

  // get pageNumbers() { // Default method with fill realization for use with ngFor
  //   const data = this.repository.getProducts(this.selectedCategory).length / this.itemsPerPage;
  //   return Array(Math.ceil(data)).fill(0).map((x, i) => i + 1);
  // }

  get pageCount(): number { // Optimized method for using with custom directive
    return Math.ceil(this.repository.getProducts(this.selectedCategory).length / this.itemsPerPage)
  }

  addProductToCart(product: Product) {
    this.cart.addLine(product);

  }

  showCart(): void {
    this.router.navigateByUrl("/cart");
  }

}

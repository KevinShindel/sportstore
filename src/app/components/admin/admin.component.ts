import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../services/auth.services";
import {Router} from "@angular/router";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  constructor(public authService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  backToStore() {
    this.router.navigateByUrl("/store", {}).then();
  }
}

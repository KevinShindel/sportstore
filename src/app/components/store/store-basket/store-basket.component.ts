import { Component, OnInit } from '@angular/core';
import {Cart} from "../../../model/cart.model";

@Component({
  selector: 'app-store-basket',
  templateUrl: './store-basket.component.html',
  styleUrls: ['./store-basket.component.css']
})
export class StoreBasketComponent implements OnInit {

  constructor(public cart: Cart) { }


  ngOnInit(): void {
  }

}

import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Cart} from "../../model/cart.model";

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class CartComponent implements OnInit {

  constructor(public cart: Cart) { }

  ngOnInit(): void {
  }

}

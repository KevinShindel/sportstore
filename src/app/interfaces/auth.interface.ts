
export interface AuthInterface {
  success: boolean;
  token: string;
  error?: string;
}

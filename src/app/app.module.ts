import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {StoreComponent} from "./components/store/store.component";
import {ProductRepository} from "./repository/product.repository";
import {StaticDataSource} from "./model/static.dataSource.service";
import {ToastrService} from "ngx-toastr";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {CounterDirective} from "./directive/counter.directive";
import {Cart} from "./model/cart.model";
import {CartComponent} from './components/cart/cart.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {CheckoutComponent} from './components/checkout/checkout.component';
import {StoreBasketComponent} from './components/store/store-basket/store-basket.component';
import {OrderRepository} from "./repository/order.repository";
import {Order} from "./model/order.model";
import {AdminModule} from "./components/admin/admin.module";
import {RestDataSource} from "./model/rest.dataSource";
import {AuthService} from "./services/auth.services";
import {HttpClientModule} from "@angular/common/http";

@NgModule({
  declarations: [
    AppComponent,
    StoreComponent,
    CounterDirective,
    CartComponent,
    CheckoutComponent,
    StoreBasketComponent,
  ],
  imports: [
    HttpClientModule,
    FontAwesomeModule,
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    CommonModule,
    AdminModule
  ],
  providers: [ // Add all Injectable service
    Order,
    OrderRepository,
    ProductRepository,
    {provide: StaticDataSource, useClass: RestDataSource},
    ToastrService,
    RestDataSource,
    AuthService,
    Cart],
  bootstrap: [AppComponent]
})
export class AppModule { }

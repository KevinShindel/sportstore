import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Product} from "./product.model";
import {Order} from "./order.model";
import {map} from "rxjs/operators";
import {NgForm} from "@angular/forms";

const PROTOCOL = "http";
const PORT = 3500;


@Injectable()
export class RestDataSource {

  private readonly baseUrl: string = '';
  public auth_token: (string | null) = null;

  constructor(private http: HttpClient) {
    this.baseUrl = `${PROTOCOL}://${location.hostname}:${PORT}/`;
  }

  getProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(this.baseUrl + "products");
  }

  saveOrder(order: Order): Observable<Order> {
    return this.http.post<Order>(this.baseUrl + "orders", order);
  }

  authenticate(form: NgForm): Observable<boolean> {
    return this.http.post<any>(this.baseUrl + "login", form.value)
      .pipe(map(response => {
        this.auth_token = response.success ? response.token : null;
        return response.success;
    }));
  }



}

import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {AdminAuthComponent} from "./admin-auth/admin-auth.component";
import {AdminComponent} from "./admin.component";
import {AuthGuard} from "../../guards/auth.guard";

@NgModule({
  imports: [CommonModule,
    FormsModule,
    RouterModule.forChild([
      {path: "auth", component: AdminAuthComponent},
      {path: "main", component: AdminComponent, canActivate: [AuthGuard]},
      {path: "**", redirectTo: "auth"},
    ])],
  providers: [AuthGuard],
  declarations: [AdminAuthComponent]
})
export class AdminModule{}
